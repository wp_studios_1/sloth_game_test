using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

public class Player_Movement : MonoBehaviour
{
// Start is called before the first frame update
  public Transform leftArmIKTarget;
  public Transform rightArmIKTarget;
  public Transform leftLegIKTarget;
  public Transform rightLegIKTarget;
  public Transform playerSpine;
  public float moveSpeed = 5f;
  public Camera mainCamera;
  public UnityEngine.Vector2 cameraOffset;
  public float smoothSpeed = 5.0f;// drag for the limbs
  public UnityEngine.Vector2 targetPosition; // stores the target position where we want the IK solvers to move to
  

  private bool leftArmSelected = false;
  private bool rightArmSelected = false;
  private bool leftLegSelected = false;
  private bool rightLegSelected = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {        
        //moved the update to the target position to the MoveSelectedLimb function so we don't have to compute the target position every frame, only when you are using a selected limb
        if(Input.GetKeyDown(KeyCode.Q))
        {
            rightArmSelected = !rightArmSelected;
        }
        if(Input.GetKeyUp(KeyCode.Q))
        {
            rightArmSelected = !rightArmSelected;
        }
        if(Input.GetKeyDown(KeyCode.W))
        {
            leftArmSelected = !leftArmSelected;
        }
        if(Input.GetKeyUp(KeyCode.W)){
            leftArmSelected = !leftArmSelected;
        }
        if(Input.GetKeyDown(KeyCode.A))
        {
            rightLegSelected = !rightLegSelected;
        }  
        if(Input.GetKeyUp(KeyCode.A))
        {
            rightLegSelected = !rightLegSelected;
        }
        if(Input.GetKeyDown(KeyCode.S))
        {
            leftLegSelected = !leftLegSelected;
        }  
        if(Input.GetKeyUp(KeyCode.S))
        {
            leftLegSelected = !leftLegSelected;
        }


        if(leftArmSelected)
        {
            MoveSelectedLimb(leftArmIKTarget);
        }
        if(rightArmSelected)
        {
            MoveSelectedLimb(rightArmIKTarget);
        }
        if(rightLegSelected)
        {
            MoveSelectedLimb(rightLegIKTarget);
        }
        if(leftLegSelected)
        {
            MoveSelectedLimb(leftLegIKTarget);
        }
    }

    private void LateUpdate()
    {
        CameraFollow();
    }

    private void CameraFollow()
    {
        if(mainCamera != null && playerSpine != null){
            UnityEngine.Vector3 desiredCamerapostion = new UnityEngine.Vector3(playerSpine.position.x, playerSpine.position.y, mainCamera.transform.position.z);
            mainCamera.transform.position = desiredCamerapostion;
        }
    }

    private void MoveSelectedLimb(Transform selectedLimb)
    {
        //Get the mouse position in screen coordinates
        UnityEngine.Vector3 mousePosition = Input.mousePosition;
        //Convert screen position to world coordinates
        UnityEngine.Vector3 worldMousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        UnityEngine.Vector2 wordlMousePosition2d = new UnityEngine.Vector2(worldMousePosition.x, worldMousePosition.y);
        targetPosition = wordlMousePosition2d;

        UnityEngine.Vector2 currentPos = new UnityEngine.Vector2(selectedLimb.position.x, selectedLimb.position.y);
        UnityEngine.Vector2 newPos = UnityEngine.Vector2.Lerp(currentPos,targetPosition, smoothSpeed * Time.deltaTime);
        //Move the selected limb IK target to the mouse position
        selectedLimb.position = new UnityEngine.Vector3(newPos.x,newPos.y, selectedLimb.position.z);
    }

}